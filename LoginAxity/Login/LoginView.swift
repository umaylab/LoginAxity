//
//  LoginView.swift
//  LoginAxity
//
//  Created by MacUmay on 19/10/22.
//

import SwiftUI

struct VisualEffectView: UIViewRepresentable{
    var effect: UIVisualEffect?
    func makeUIView(context: UIViewRepresentableContext<Self>) -> UIVisualEffectView {
        UIVisualEffectView()
    }
    func updateUIView(_ uiView: UIVisualEffectView, context: UIViewRepresentableContext<Self>) {
        uiView.effect = effect
    }
}


struct Login: View {
    
    @State private var showAlert: Bool = false
    @ObservedObject var login = LoginNet()
    @State private var email = ""
    @State private var pass = ""
    @Binding var logeado : Bool
    
    var alert: Alert {
        Alert(title: Text("Alert"), message: Text("Your Email y/o password are not in our database please validate again"), dismissButton: .default(Text("Accept")
            ))
    }
    
    var body: some View {
        
        ZStack{
            
            Image("background")
                .resizable()
                .scaledToFill()
                .edgesIgnoringSafeArea(.all)
            
            VisualEffectView(effect:UIBlurEffect(style: .regular))
            
            
            VStack{
               VStack{
                Image("avatar")
                   Text("Login")
                       .font(.largeTitle)
                       .fontWeight(.light)
                       .foregroundColor(.black)
               }.offset(y: 60)
                   
               HStack (alignment: .center, spacing: 30){
                   Image(systemName: "envelope.fill")
                   TextField("Email", text: self.$email)
                   .frame(width: 250, height: 50)
               }.offset(y: 100)
               
               HStack(alignment: .center, spacing: 30){
                   Image(systemName: "lock.fill")
                   SecureField("Password", text: self.$pass)
                       .frame(width: 250, height: 50)
                   
               }.offset(y: 100)
               
               Button(action:{
                   self.login.loginDetail(email: self.email, pass: self.pass)
                           if self.login.authenticated == 1 {
                            self.logeado = true
                               print("Correct data you have logged in")
                               
                           }else if self.login.authenticated == 2{
                                print("User y/o invalid password")
                                self.showAlert.toggle()
                              }
                }){
                       Text("Enter")
                       .frame(width: 300, height: 40)
               } .padding(50)
                  .frame(width: 300, height: 40)
                  .background(Color.black)
                  .cornerRadius(10)
                  .foregroundColor(.white)
                  .offset(y: 200)
           .padding()
           Spacer()
            }
        
        }.alert(isPresented: self.$showAlert, content: {self.alert})
    }
}



struct Login_Previews: PreviewProvider {
    static var previews: some View {
        Login(logeado: .constant(false))
    }
}
