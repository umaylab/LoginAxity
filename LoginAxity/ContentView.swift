//
//  ContentView.swift
//  LoginAxity
//
//  Created by MacUmay on 19/10/22.
//

import SwiftUI

struct ContentView: View {
    
    @State var logeado = false
    var body: some View {
        return Group{
            if  logeado {
                User(logeado: self.$logeado)
            }else{
                Login(logeado: self.$logeado)
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
