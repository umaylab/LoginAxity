//
//  UserList.swift
//  LoginAxity
//
//  Created by MacUmay on 19/10/22.
//

import Foundation

struct UserList: Identifiable {
    var id : Int
    var first_name : String
    var last_name: String
    
    var email: String
    var avatar: String
}
